from django.conf.urls import url, include
from web_interface import views

urlpatterns = [

	url(r'^login/$', views.login),
	url(r'^logout/$', views.logout),

	url(r'^manager/(?P<manager_id>\w+)/$', views.manager),
	url(r'^manager/$', views.manager),

	url(r'^category/(?P<category>\w+)/(?P<category_id>[0-9])/$', views.category),
	url(r'^category/(?P<category>\w+)/$', views.category),
	url(r'^category/$', views.category),

	url(r'^city/(?P<city_id>\w+)/$', views.city),
	url(r'^city/$', views.city),

	url(r'^master/(?P<master_id>\w+)/$', views.master),
	url(r'^master/$', views.master),

	url(r'^tattoo/(?P<tattoo_id>\w+)/$', views.tattoo),
	url(r'^tattoo/$', views.tattoo),
	url(r'^$', views.tattoo),
]
