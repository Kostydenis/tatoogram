from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from core.models import *
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.core.paginator import Paginator
from django.contrib.auth.models import User
import string
from random import sample, choice

# Create your views here.

TATTOOS_ON_PAGE = 10
MASTERS_ON_PAGE = 10

def login(request):
	if request.method == 'GET':
		if not request.user.is_authenticated():
			return render_to_response('login.html')
		else:
			return HttpResponseRedirect('/')
	elif request.method == 'POST':

		username = request.POST['username']
		password = request.POST['password']

		if not request.POST.get('remember_me', None):
			request.session.set_expiry(0)

		user = authenticate(username=username, password=password)
		if user is not None:
			django_login(request, user)
			return HttpResponseRedirect('/')
		else:
			res_options = {}
			res_options['status'] = 'invalid_login'
			return render_to_response('login.html', res_options)

def logout(request):
	django_logout(request)
	return HttpResponseRedirect('/')

def manager(request, manager_id = None):
	def get_manager(request, manager_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		res_options['managers'] = User.objects.all()

		return render_to_response('managers.html', res_options)

	def post_manager(request, manager_id):
		action = request.POST.get('action', None)

		if manager_id is not None:
			try:
				obj = User.objects.get(id=manager_id)
			except ObjectDoesNotExist:
				raise Http404("City does not exist.")

			if action == 'edit':

				obj.set_password(request.POST.get('password', None))

				obj.save()
				return HttpResponseRedirect('/login')

			elif action == 'delete':
				result = obj.delete()
				return get_manager(request, None, status='deleted')
		else:

			username = request.POST.get('username', None)
			password = ''.join(choice(string.ascii_letters + string.digits) for _ in range(8))
			user, created = User.objects.get_or_create(username = username, defaults = {
				'password': password,
				'email' : ''
			})
			if created:
				user.set_password(password)
				user.save()
			else:
				return get_manager(request, None, status='error')

			status = {}
			status['new_login'] = username
			status['new_password'] = password
			return get_manager(request, None, status=status)

	if request.method == 'GET':
		return get_manager(request, manager_id)
	else:
		return post_manager(request, manager_id)

def category(request, category = None, category_id = None):

	def get_tattoo(request, category, category_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		page = request.GET.get('page', 1)

		if category == 'content':
			raw_data = Tattoo.objects.filter(content_id=category_id)
			res_options['page_name'] = 'Изображения в категории ' + str(dict_Content.objects.get(id=category_id))
		if category == 'style':
			raw_data = Tattoo.objects.filter(style_id=category_id)
			res_options['page_name'] = 'Изображения в категории ' + str(dict_Style.objects.get(id=category_id))
		if category == 'bodypart':
			raw_data = Tattoo.objects.filter(bodypart_id=category_id)
			res_options['page_name'] = 'Изображения в категории ' + str(dict_BodyPart.objects.get(id=category_id))

		paginator = Paginator(raw_data, TATTOOS_ON_PAGE)

		try:
			res_options['tattoos'] = paginator.page(page)
		except PageNotAnInteger:
			res_options['tattoos'] = paginator.page(1)
		except EmptyPage:
			res_options['tattoos'] = paginator.page(paginator.num_pages)

		return render_to_response('tattoos.html', res_options)


	def get_category(request, category, category_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		if category is not None:
			return get_tattoo(request, category, category_id)
		else:
			res_options['contents']  = dict_Content.objects.all()
			res_options['styles']    = dict_Style.objects.all()
			res_options['bodyparts'] = dict_BodyPart.objects.all()

			return render_to_response('categories.html', res_options)

	def post_category(request, category, category_id):
		action = request.POST.get('action', None)

		if category_id is not None:
			if category == 'content':
				try:
					obj = dict_Content.objects.get(id=category_id)
				except ObjectDoesNotExist:
					raise Http404("Content does not exist.")
			if category == 'style':
				try:
					obj = dict_Style.objects.get(id=category_id)
				except ObjectDoesNotExist:
					raise Http404("Style does not exist.")
			if category == 'bodypart':
				try:
					obj = dict_BodyPart.objects.get(id=category_id)
				except ObjectDoesNotExist:
					raise Http404("Bodypart does not exist.")

			if action == 'edit':

				obj.value = request.POST.get('value', None)

				obj.save()
				return get_category(request, None, None, status='edited')

			elif action == 'delete':
				if category == 'content':
					raw_data = Tattoo.objects.filter(content_id=category_id)
					for item in raw_data:
						item.content_id = None
						item.save()
				if category == 'style':
					raw_data = Tattoo.objects.filter(style_id=category_id)
					for item in raw_data:
						item.style_id = None
						item.save()
				if category == 'bodypart':
					raw_data = Tattoo.objects.filter(bodypart_id=category_id)
					for item in raw_data:
						item.bodypart_id = None
						item.save()
				result = obj.delete()
				return get_category(request, None, None, status='deleted')
		else:
			value = request.POST.get('value', None)

			if category == 'content':
				obj = dict_Content(value = value)
			if category == 'style':
				obj = dict_Style(value = value)
			if category == 'bodypart':
				obj = dict_BodyPart(value = value)
			obj.save()

			if obj.id is None:
				raise Http400("Something went wrong")
			else:
				return get_category(request, None, None, status='added')

	if request.method == 'GET':
		return get_category(request, category, category_id)
	else:
		return post_category(request, category, category_id)

def city(request, city_id = None):

	def get_masters(request, city_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		page = request.GET.get('page', 1)

		raw_data = Master.objects.filter(city_id=city_id)
		paginator = Paginator(raw_data, MASTERS_ON_PAGE)

		try:
			res_options['masters'] = paginator.page(page)
		except PageNotAnInteger:
			res_options['masters'] = paginator.page(1)
		except EmptyPage:
			res_options['masters'] = paginator.page(paginator.num_pages)

		res_options['page_name'] = 'Мастера в городе ' + str(dict_City.objects.get(id=city_id))
		return render_to_response('masters.html', res_options)

	def get_city(request, city_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		if city_id is not None:
			return get_masters(request, city_id)
		else:
			res_options['cities'] = dict_City.objects.all()
			return render_to_response('cities.html', res_options)

	def post_city(request, city_id):
		action = request.POST.get('action', None)

		if city_id is not None:
			try:
				obj = dict_City.objects.get(id=city_id)
			except ObjectDoesNotExist:
				raise Http404("City does not exist.")

			if action == 'edit':

				obj.value = request.POST.get('value', None)

				obj.save()
				return get_city(request, None, status='edited')

			elif action == 'delete':
				result = obj.delete()
				return get_city(request, None, status='deleted')
		else:
			value = request.POST.get('value', None)

			obj = dict_City(
				value = value,
			)
			obj.save()

			if obj.id is None:
				raise Http400("Something went wrong")
			else:
				return get_city(request, None, status='added')

	if request.method == 'GET':
		return get_city(request, city_id)
	else:
		return post_city(request, city_id)

def master(request, master_id = None):

	def get_master(request, master_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		is_add = request.GET.get('action', None)
		if is_add == 'create':
			if request.user.is_authenticated:

				res_options['styles'] = dict_Style.objects.all()
				res_options['cities'] = dict_City.objects.all()

				return render_to_response('add_master.html', res_options)
			else:
				return HttpResponseRedirect('/login')
		else:
			if master_id is not None:
				try:
					raw_data = Master.objects.get(id=master_id)
				except ObjectDoesNotExist:
					raise Http404("Master does not exist.")

				res_options['page_name'] = 'Мастер ' + str(raw_data.name)
				res_options['master'] = raw_data

				if request.user.is_authenticated:
					res_options['styles'] = dict_Style.objects.all()
					res_options['cities'] = dict_City.objects.all()

					return render_to_response('master.html', res_options)
				else:
					return render_to_response('master.html', res_options)
			else:
				page = request.GET.get('page', 1)

				raw_data = Master.objects.all().reverse()
				paginator = Paginator(raw_data, MASTERS_ON_PAGE)

				try:
					res_options['masters'] = paginator.page(page)
				except PageNotAnInteger:
					res_options['masters'] = paginator.page(1)
				except EmptyPage:
					res_options['masters'] = paginator.page(paginator.num_pages)

				res_options['page_name'] = 'Список мастеров'
				return render_to_response('masters.html', res_options)


	def post_master(request, master_id):
		action = request.POST.get('action', None)

		if master_id is not None:
			try:
				obj = Master.objects.get(id=master_id)
			except ObjectDoesNotExist:
				raise Http404("Master does not exist.")

			if action == 'edit':

				obj = Master.objects.get(id=master_id)

				obj.name = request.POST.get('name', None)

				is_del = request.POST.get('deletephoto', None)
				if is_del is not None:
					obj.photo.delete()
				else:
					try:
						img = request.FILES['img']
					except MultiValueDictKeyError:
						pass
					else:
						if img is not None:
							obj.photo.delete()
							obj.photo = img

				obj.about = request.POST.get('about', None)
				obj.city_id = request.POST.get('city_id', None)
				obj.address = request.POST.get('address', None)
				obj.phone = request.POST.get('phone', None)
				obj.mail = request.POST.get('mail', None)
				obj.site = request.POST.get('site', None)
				obj.vk = request.POST.get('vk', None)
				obj.facebook = request.POST.get('facebook', None)
				obj.instagram = request.POST.get('instagram', None)
				obj.whatsapp = request.POST.get('whatsapp', None)
				obj.viber = request.POST.get('viber', None)

				style_of_choice = request.POST.getlist('style_ids')
				obj.style_of_choice.clear()
				obj.style_of_choice.add(*style_of_choice)

				comment = request.POST.get('comment', None)
				# if comment is not None:
				# 	obj.comment = comment

				obj.save()
				return get_master(request, obj.id, status='edited')

			elif action == 'delete':
				obj.photo.delete()
				result = obj.delete()
				return get_master(request, master_id=None, status='deleted')
		else:
			name = request.POST.get('name', None)

			try:
				img = request.FILES['img']
			except MultiValueDictKeyError:
				pass

			about = request.POST.get('about', None)
			city_id = request.POST.get('city_id', None)
			address = request.POST.get('address', None)
			phone = request.POST.get('phone', None)
			mail = request.POST.get('mail', None)
			site = request.POST.get('site', None)
			vk = request.POST.get('vk', None)
			facebook = request.POST.get('facebook', None)
			instagram = request.POST.get('instagram', None)
			whatsapp = request.POST.get('whatsapp', None)
			viber = request.POST.get('viber', None)

			comment = request.POST.get('comment', None)

			obj = Master(
				name = name,
				about = about,
				city_id = city_id,
				address = address,
				phone = phone,
				mail = mail,
				site = site,
				vk = vk,
				facebook = facebook,
				instagram = instagram,
				whatsapp = whatsapp,
				viber = viber,
				photo = img
			)
			obj.save()
			style_of_choice = request.POST.getlist('style_ids')
			obj.style_of_choice.add(*style_of_choice)

			if obj.id is None:
				raise Http400("Something went wrong")
			else:
				return get_master(request, obj.id, status='added')

	if request.method == 'GET':
		return get_master(request, master_id)
	else:
		return post_master(request, master_id)

def tattoo(request, tattoo_id = None):

	def get_tattoo(request, tattoo_id, status = None):
		res_options = {}
		res_options['user'] = request.user
		res_options['status'] = status

		is_add = request.GET.get('action', None)
		if is_add == 'create':
			if request.user.is_authenticated:

				res_options['contents']  = dict_Content.objects.all()
				res_options['styles']    = dict_Style.objects.all()
				res_options['bodyparts'] = dict_BodyPart.objects.all()
				res_options['masters']   = Master.objects.all()

				return render_to_response('add_tattoo.html', res_options)
			else:
				return HttpResponseRedirect('/login')
		else:
			if tattoo_id is not None:
				try:
					raw_data = Tattoo.objects.get(id=tattoo_id)
				except ObjectDoesNotExist:
					raise Http404("Tattoo does not exist.")

				res_options['page_name'] = 'Изображение ' + str(tattoo_id)
				res_options['tattoo'] = raw_data

				if request.user.is_authenticated:

					res_options['contents']  = dict_Content.objects.all()
					res_options['styles']    = dict_Style.objects.all()
					res_options['bodyparts'] = dict_BodyPart.objects.all()
					res_options['masters']   = Master.objects.all()

					return render_to_response('tattoo.html', res_options)
				else:
					return render_to_response('tattoo.html', res_options)
			else:
				page = request.GET.get('page', 1)

				raw_data = Tattoo.objects.all().order_by('-created_when')
				paginator = Paginator(raw_data, TATTOOS_ON_PAGE)

				try:
					res_options['tattoos'] = paginator.page(page)
				except PageNotAnInteger:
					res_options['tattoos'] = paginator.page(1)
				except EmptyPage:
					res_options['tattoos'] = paginator.page(paginator.num_pages)

				res_options['page_name'] = 'Главная страница'
				return render_to_response('tattoos.html', res_options)


	def post_tattoo(request, tattoo_id):
		action = request.POST.get('action', None)

		if tattoo_id is not None:
			try:
				obj = Tattoo.objects.get(id=tattoo_id)
			except ObjectDoesNotExist:
				raise Http404("Tattoo does not exist.")

			if action == 'edit':

				obj = Tattoo.objects.get(id=tattoo_id)

				obj.content_id  = request.POST.get('content_id', None)
				obj.style_id    = request.POST.get('style_id', None)
				obj.bodypart_id = request.POST.get('bodypart_id', None)

				obj.master_id = request.POST.get('master_id', None)

				comment = request.POST.get('comment', None)
				# if comment is not None:
				# 	obj.comment = comment

				obj.save()
				return get_tattoo(request, obj.id, status='edited')

			elif action == 'delete':
				obj.img.delete()
				result = obj.delete()
				if result[0] == 1:
					return get_tattoo(request, tattoo_id=None, status='deleted')
				else:
					return get_tattoo(request, obj.id, status='error')
		else:
			content = request.POST.get('content_id', None)
			if (content is None) or (content == ''):
				content = None
			else:
				content = dict_Content.objects.get(id=content)

			style = request.POST.get('style_id', None)
			if (style is None) or (style == ''):
				style = None
			else:
				style = dict_Style.objects.get(id=style)

			bodypart = request.POST.get('bodypart_id', None)
			if (bodypart is None) or (bodypart == ''):
				bodypart = None
			else:
				bodypart = dict_BodyPart.objects.get(id=bodypart)

			master = request.POST.get('master_id', None)
			if (master is None) or (master == ''):
				master = None
			else:
				master = Master.objects.get(id=master)

			img          = request.FILES['img']
			comment      = request.POST.get('comment', None)
			created_when = datetime.datetime.now()
			created_by   = request.user

			obj = Tattoo(
				content = content,
				style = style,
				bodypart = bodypart,
				master = master,
				img = img,
				comment = comment,
			)

			obj.save()
			if obj.id is None:
				raise Http400("Something went wrong")
			else:
				return get_tattoo(request, obj.id, status='added')

	if request.method == 'GET':
		return get_tattoo(request, tattoo_id)
	else:
		return post_tattoo(request, tattoo_id)

