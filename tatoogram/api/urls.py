from django.conf.urls import url, include
from api import views_v1

urlpatterns = [

	url(r'^v1/', include([

		url(r'^about/', views_v1.about),

		url(r'^tattoo/', include([
			url(r'^(?P<tattoo_id>\w+)/$', views_v1.tattoo),
			url(r'^$', views_v1.tattoo),
		])),

		url(r'^city/', include([
			url(r'^(?P<city_id>\w+)/$', views_v1.city),
			url(r'^$', views_v1.city),
		])),

		url(r'^master/', include([
			url(r'^(?P<master_id>\w+)/$', views_v1.master),
			url(r'^$', views_v1.master),
		])),

		url(r'^portfolio/', include([
			url(r'^(?P<portfolio_id>\w+)/$', views_v1.portfolio),
			url(r'^$', views_v1.portfolio),
		])),

		url(r'^content/', include([
			url(r'^(?P<content_id>\w+)/$', views_v1.content),
			url(r'^$', views_v1.content),
		])),

		url(r'^style/', include([
			url(r'^(?P<style_id>\w+)/$', views_v1.style),
			url(r'^$', views_v1.style),
		])),

		url(r'^bodypart/', include([
			url(r'^(?P<bodypart_id>\w+)/$', views_v1.bodypart),
			url(r'^$', views_v1.bodypart),
		])),

	])),
]
