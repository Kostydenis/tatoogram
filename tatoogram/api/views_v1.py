from django.shortcuts import render
from core.models import *

from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse
import json
import random

### DECORATORS ###

def only_get_requests(function):

	def wrap(request, *args, **kwargs):

		if request.method == 'GET':
			return function(request, *args, **kwargs)

		else:
			return json_to_response(
					status_code = 405,
					status_message='GET requests is allowed only'
				)

	return wrap


### END DECORATORS ###

### SERVICE ###
def json_to_response(data=None, status_code=200, status_message='undefined'):

	formatted_data = {}

	if data is not None:
		formatted_data['data'] = data

	formatted_data['status_code'] = status_code
	if (status_code == 200 or
		status_code == 201) and status_message == 'undefined':
		formatted_data['status_message'] = 'success'
	else:
		formatted_data['status_message'] = status_message

	jsoned = json.dumps(formatted_data, ensure_ascii=False).encode('utf8')
	return HttpResponse(jsoned,
		content_type='application/json',
		status=status_code,
	)

def extract_tattoos(where=None, dict_id=None, tattoo_id=None):

	output = []
	tattoos = []

	if where is None:

		if tattoo_id is None:

			tattoos = Tattoo.objects.all()

		else:
			try:
				tattoos.append(Tattoo.objects.get(id=tattoo_id))
			except ObjectDoesNotExist:
				raise

	else:

		if where.__name__ == 'dict_Style':
			tattoos = Tattoo.objects.filter(style=dict_id)

		elif where.__name__ == 'dict_Content':
			tattoos = Tattoo.objects.filter(content=dict_id)

		elif where.__name__ == 'dict_BodyPart':
			tattoos = Tattoo.objects.filter(bodypart=dict_id)

	if len(tattoos) == 1:
		return format_tattoo(tattoos[0])
	else:
		for item in tattoos:
			output.append(format_tattoo(item))

	return output

def format_dict(src):
	"""convert query set to a field of JSON"""

	raw_data = src.objects.all()

	values = []
	for dict in raw_data:
		item = {}
		item['name'] = str(dict)
		item['id'] = dict.id

		# random img
		if raw_data[0].__class__.__name__ == 'dict_Content':
			query = Tattoo.objects.filter(content=dict.id)
		elif raw_data[0].__class__.__name__ == 'dict_Style':
			query = Tattoo.objects.filter(style=dict.id)
		elif raw_data[0].__class__.__name__ == 'dict_BodyPart':
			query = Tattoo.objects.filter(bodypart=dict.id)

		count = query.count()

		if count == 0:
			pass
		elif count == 1:
			pos = 0
			item['random_img'] = format_tattoo(query[pos])
		else:
			pos = random.randint(0,count-1)
			item['random_img'] = format_tattoo(query[pos])

		values.append(item)

	return values

def format_tattoo(obj, short=False):

	output = {}

	output['id']        = str(obj.id)
	output['img']       = str(obj.img)
	output['thumbnail'] = str(obj.thumbnail)

	if not short:
		output['img_width']    = str(obj.img_width)
		output['img_height']   = str(obj.img_height)
		output['img_ratio']    = str(obj.img_ratio)

		output['thumb_width']  = str(obj.thumb_width)
		output['thumb_height'] = str(obj.thumb_height)

		output['content']      = str(obj.content)
		output['style']        = str(obj.style)
		output['bodypart']     = str(obj.bodypart)
		output['master']       = str(obj.master)

	return output

def format_master(obj, short=False):

	output = {}

	output['id']    = str(obj.id)
	output['name'] = str(obj.name)
	output['photo'] = str(obj.photo)

	if not short:

		if obj.about is not None and obj.about != '':
			output['about'] = str(obj.about)
		if obj.city is not None and obj.city != '':
			output['city'] = str(obj.city)
		if obj.address is not None and obj.address != '':
			output['address'] = str(obj.address)
		if obj.phone is not None and obj.phone != '':
			output['phone'] = str(obj.phone)
		if obj.mail is not None and obj.mail != '':
			output['mail'] = str(obj.mail)
		if obj.site is not None and obj.site != '':
			output['site'] = str(obj.site)
		if obj.vk is not None and obj.vk != '':
			output['vk'] = str(obj.vk)
		if obj.facebook is not None and obj.facebook != '':
			output['facebook'] = str(obj.facebook)
		if obj.instagram is not None and obj.instagram != '':
			output['instagram'] = str(obj.instagram)
		if obj.whatsapp is not None and obj.whatsapp != '':
			output['whatsapp'] = str(obj.whatsapp)
		if obj.viber is not None and obj.viber != '':
			output['viber'] = str(obj.viber)

		output['style_of_choice'] = []
		for item in obj.style_of_choice.all():
			curr_style = {}
			curr_style['id'] = item.id
			curr_style['name'] = item.value
			output['style_of_choice'].append(curr_style)

	return output

### END SERVICE ###

### VIEWS ###

@only_get_requests
def about(request, tattoo_id=None):
	pass

@only_get_requests
def tattoo(request, tattoo_id=None):

	if tattoo_id is None:
		return json_to_response(extract_tattoos())

	else:
		try:
			return json_to_response(extract_tattoos(tattoo_id=tattoo_id))
		except ObjectDoesNotExist:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no tattoo with requested id'
			)

@only_get_requests
def master(request, master_id=None):
	output = []
	raw_data = []

	if master_id is None:
		raw_data = Master.objects.all()
	else:
		try:
			raw_data.append(Master.objects.get(id=master_id))
		except ObjectDoesNotExist:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no master with requested id'
			)
	for item in raw_data:
		output.append(format_master(item))

	return json_to_response(output)

@only_get_requests
def city(request, city_id=None):
	output = []
	raw_data = []

	if city_id is None:
		raw_data = dict_City.objects.all()

		for item in raw_data:
			output.append(
				{
					'id': str(item.id),
					'name': str(item)
				}
			)
	else:
		output = {}
		output['masters'] = []

		# if there's no city with requested id, send error message
		output = {}
		output['masters'] = []
		try:
			raw_data = dict_City.objects.get(id=city_id)
		except ObjectDoesNotExist:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no city with requested id'
			)
		output['id'] = str(raw_data.id)
		output['name'] = str(raw_data)

		raw_data = Master.objects.filter(city=city_id)
		for item in raw_data:
			output['masters'].append(format_master(item))

	return json_to_response(output)

@only_get_requests
def portfolio(request, portfolio_id=None):
	output = []
	raw_data = []

	if portfolio_id is None:
		return json_to_response(
				data=[],
				status_code=400,
				status_message='Request must contain id of master'
			)
	else:
		try:
			Master.objects.get(id=portfolio_id)
		except ObjectDoesNotExist:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no master with requested id'
			)

		raw_data = Tattoo.objects.filter(master=portfolio_id)
		if len(raw_data) == 0:
			return json_to_response(
				data=[],
				status_code=200,
				status_message='Master has empty portfolio'
			)
		else:
			for item in raw_data:
				output.append(format_tattoo(item))

	return json_to_response(output)

@only_get_requests
def content(request, content_id=None):

	if content_id is None:

		return json_to_response(format_dict(dict_Content))

	else:

		response = {}
		response['tattoos'] = extract_tattoos(where=dict_Content, dict_id=content_id)

		if len(response['tattoos']) == 0:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no content with requested id or tattoo with requested content'
			)

		response['name'] = str(dict_Content.objects.get(id=content_id))

		return json_to_response(response)

@only_get_requests
def style(request, style_id=None):

	if style_id is None:

		return json_to_response(format_dict(dict_Style))

	else:

		response = {}
		response['tattoos'] = extract_tattoos(where=dict_Style, dict_id=style_id)

		if len(response['tattoos']) == 0:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no style with requested id or tattoo with requested style'
			)

		response['name'] = str(dict_Style.objects.get(id=style_id))

		return json_to_response(response)

@only_get_requests
def bodypart(request, bodypart_id=None):

	if bodypart_id is None:

		return json_to_response(format_dict(dict_BodyPart))

	else:
		response = {}
		response['tattoos'] = extract_tattoos(where=dict_BodyPart, dict_id=bodypart_id)

		if len(response['tattoos']) == 0:
			return json_to_response(
				data=[],
				status_code=404,
				status_message='There\'s no bodypart with requested id or tattoo with requested bodypart'
			)

		response['name'] = str(dict_BodyPart.objects.get(id=bodypart_id))

		return json_to_response(response)
### END VIEWS ###
