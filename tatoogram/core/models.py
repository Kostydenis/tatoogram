from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import datetime
import uuid

from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

### DICTS ###

class dict_Content(models.Model):
	"""Category"""
	value = models.CharField(max_length=255)
	def __str__(self):
		return self.value

class dict_Style(models.Model):
	"""Style"""
	value = models.CharField(max_length=255)
	def __str__(self):
		return self.value

class dict_BodyPart(models.Model):
	"""Part of body"""
	value = models.CharField(max_length=255)
	def __str__(self):
		return self.value

class dict_City(models.Model):
	"""Cities"""
	value = models.CharField(max_length=255)
	def __str__(self):
		return self.value

### END DICTS ###

### MASTERS ###
def avatar_path(instance, filename):
	output = 'masters/'
	output += str(uuid.uuid4())
	output += '.' + filename.split('.')[-1] # appending extension
	return output
class Master(models.Model):
	"""
		Master class.
		Don't know how to integrate with auth.User
	"""

	name            = models.CharField(max_length=255, null=True, blank=True)

	photo           = models.ImageField(upload_to=avatar_path, null=True, blank=True)

	about           = models.TextField(blank=True, null=True)
	city            = models.ForeignKey(dict_City)
	address         = models.CharField(max_length=100, blank=True, null=True)
	phone           = models.CharField(max_length=20, blank=True, null=True)
	mail            = models.CharField(max_length=100, blank=True, null=True)

	site            = models.CharField(max_length=100, blank=True, null=True)
	vk              = models.CharField(max_length=100, blank=True, null=True)
	facebook        = models.CharField(max_length=100, blank=True, null=True)
	instagram       = models.CharField(max_length=100, blank=True, null=True)
	whatsapp        = models.CharField(max_length=100, blank=True, null=True)
	viber           = models.CharField(max_length=100, blank=True, null=True)

	# master's preferable styles
	style_of_choice = models.ManyToManyField(dict_Style)

	def __str__(self):
		return self.name
### END MASTERS ###

### MAIN TATTOO CLASS ###
def tatoo_path(instance, filename):
	output = 'tattoo/%s.' %(str(uuid.uuid4())) # path with a name (tattoo id)
	output += filename.split('.')[-1] # appending extension
	return output
thumbnail_path = 'thumbnails' # path with a name (tattoo id)
class Tattoo(models.Model):

	content      = models.ForeignKey(dict_Content, blank=True, null=True)
	style        = models.ForeignKey(dict_Style, blank=True, null=True)
	bodypart     = models.ForeignKey(dict_BodyPart, blank=True, null=True)

	master       = models.ForeignKey(Master, related_name='author', blank=True, null=True)

	img_width    = models.PositiveSmallIntegerField(blank=True, null=True)
	img_height   = models.PositiveSmallIntegerField(blank=True, null=True)

	img          = models.ImageField(
						upload_to=tatoo_path,
						height_field='img_height',
						width_field='img_width',
						blank=True, null=True
					)

	img_ratio    = models.FloatField(blank=True, null=True)
	thumb_ratio  = models.FloatField(blank=True, null=True)

	thumb_width  = models.PositiveSmallIntegerField(blank=True, null=True)
	thumb_height = models.PositiveSmallIntegerField(blank=True, null=True)

	thumbnail    = models.ImageField(
						upload_to=thumbnail_path,
						height_field='thumb_height',
						width_field='thumb_width',
						blank=True, null=True
					)

	comment      = models.TextField(blank=True, null=True)
	created_when = models.DateTimeField(default=timezone.now, blank=True, null=True)
	created_by   = models.ForeignKey(User, related_name='created_by', blank=True, null=True)

@receiver(post_save, sender=Tattoo)
def generate_thumb(sender, instance, **kwargs):
	if not instance.thumbnail:

		import os
		from PIL import Image, ImageFilter
		import io
		from django.core.files.uploadedfile import SimpleUploadedFile

		### THUMBNAIL SETTINGS ###
		# Set our max thumbnail size in a tuple (max width, max height)
		THUMBNAIL_SIZE = (400, 200)
		BLUR_RADIUS = 3
		### THUMBNAIL SETTINGS ###

		ext = os.path.splitext(instance.img.name)[1][1:]
		if ext == 'jpg':
			pil_type = 'jpeg'
			django_type = 'image/jpg'
		if ext == 'png':
			pil_type = 'png'
			django_type = 'image/png'
		instance.img.open()
		image = Image.open(io.BytesIO(instance.img.read()))

		image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
		image = image.filter(ImageFilter.GaussianBlur(BLUR_RADIUS))

		temp_handle = io.BytesIO()
		image.save(temp_handle, pil_type)
		temp_handle.seek(0)
		suf = SimpleUploadedFile(
			os.path.split(instance.img.name)[-1],
			temp_handle.read(),
			content_type=django_type
		)
		instance.thumbnail.save(
			os.path.split(instance.img.name)[-1],
			suf,
			save=True
		)

		instance.img_ratio = instance.img_width / instance.img_height
		instance.thumb_ratio = instance.thumb_width / instance.thumb_height

		instance.save()
### END MAIN TATTOO CLASS ###

class About(models.Model):
	year = models.PositiveSmallIntegerField(default=2017)
