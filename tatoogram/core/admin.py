from django.contrib import admin

from core.models import dict_Content
from core.models import dict_Style
from core.models import dict_BodyPart
from core.models import dict_City
from core.models import Master
from core.models import Tattoo
from core.models import About

admin.site.register(dict_Content)
admin.site.register(dict_Style)
admin.site.register(dict_BodyPart)
admin.site.register(dict_City)
admin.site.register(Master)
admin.site.register(Tattoo)
admin.site.register(About)
